var express = require('express');
var mysql = require('mysql');
var app = express();

var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'admin',
  password : '=@!#254tecmint',
  database : 'Miniblog'
});
 
connection.connect();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/articles', (req, res) => {
	var user_id = req.query.user_id;
	var likes = req.query.likes;
	var query = 'SELECT * FROM `articles`'

	if (user_id) { query += ' WHERE user_id='+user_id }

	else if (likes == 'true') { query = 'SELECT articles.id, title, content, articles.user_id, CONCAT("[", GROUP_CONCAT( "user_id:", likes.user_id) ,"]") AS likes FROM `articles` INNER JOIN `likes` ON articles.id = likes.article_id GROUP BY articles.id';	}

	connection.query(query, function (error, results, fields) {
		if (error) throw error;
		res.json({ articles: results });
	})
});

app.post('/articles', (req, res) => {
	var query = 'INSERT INTO `articles` (`id`, `title`, `content`, `user_id`) VALUES (NULL, "'+req.body.title+'", "'+req.body.content+'", "'+req.body.user_id+'");'

	connection.query(query, function (error, results, fields) {
		if (error) throw error;
		res.json(results);
	})
})

app.get('/users', (req, res) => {
	var wroteArticles = req.query.wroteArticles;
	var likes = req.query.likes;
	var groupBy = req.query.groupBy;

	var query = 'SELECT * FROM `users`';

	if (wroteArticles == 'false') { 
		query = 'SELECT users.id, users.username, GROUP_CONCAT(articles.id) AS articles, GROUP_CONCAT(likes.id) AS likes FROM `users` LEFT JOIN articles ON users.id = articles.user_id LEFT JOIN likes ON users.id = likes.user_id GROUP BY users.id HAVING articles IS NULL';

		if (likes == 'true') {
			query += ' AND likes IS NOT NULL';
		}
	}
	
	else if (groupBy) {
		query = 'SELECT universes.type, GROUP_CONCAT(users.id) AS users FROM users INNER JOIN universes ON users.id = universes.user_id GROUP BY universes.type';

	}
	
	console.log(query);

	connection.query(query, function (error, results, fields) {
		if (error) throw error;
		res.json(results);
	})
});

app.post('/users', (req, res) => {
	var query = 'INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES (NULL, "'+req.body.username+'", "'+req.body.email+'", "'+req.body.password+'");'
	connection.query(query, function (error, results, fields) {
		if (error) throw error;

		var query2 = 'INSERT INTO `universes` (`id`, `user_id`, `type`) VALUES (NULL, "'+results.insertId+'", "'+req.body.type+'")';
		connection.query(query2, function(error2, results2, fields2) {
			res.json({ results, results2 });
		})
	})
})

app.put('/users/:id', (req, res) => {
	var user_id = req.params.id;
	var type = req.body.type;

	// we check if the user_id exists in the universes table
	var query = 'SELECT user_id FROM `universes` WHERE user_id = "'+ user_id +'"';
	connection.query(query, function(error, results, fields) {
		if (error) throw error;
		
		// if it doesn't exist, we create a new record
		if (results.length === 0) {
			var query2 = 'INSERT INTO `universes` (user_id, type)  VALUES ("'+ user_id + '", "'+ type + '")';
			connection.query(query2, function(error2, results2, fields2) {
				if (error2) throw error2;
				res.json(results2);
			})
		}
		// otherwise, we update the existing record
		else {
			var query2 = 'UPDATE `universes` SET type = "'+ type + '" WHERE user_id = "'+ results[0].user_id +'"';
			connection.query(query2, function(error2, results2, fields2) {
				if (error2) throw error2;
				res.json(results2);
			})
		}
	})

})

app.get('/top-article', (req, res) => {
	connection.query('SELECT * FROM `articles` ORDER BY `articles`.`number_of_likes` DESC LIMIT 1 ', function (error, results, fields) {
		if (error) throw error;
		res.json(results[0]);
	})
});

app.post('/likes', (req, res) => {
	var query = 'INSERT INTO `likes` (`id`, `article_id`, `user_id`) VALUES (NULL, "'+req.body.article_id+'", "'+req.body.user_id+'");'
	connection.query(query, function (error, results, fields) {
		if (error) throw error;

		var query2 = 'UPDATE`articles` SET number_of_likes = number_of_likes + 1 WHERE id = '+req.body.article_id;
		connection.query(query2, function (error2, results2, fields2) {
			if (error2) throw error2;
			res.json(results2);
		});

		res.json(results);
	})
})

app.listen(3000, () => {
	console.log('listening to the sea');
});
